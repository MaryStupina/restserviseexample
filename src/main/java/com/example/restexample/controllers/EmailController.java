package com.example.restexample.controllers;

import com.example.restexample.entity.User;
import com.example.restexample.service.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
public class EmailController {

    @Autowired
    private EmailService emailService;

//    @GetMapping("/example/{firstValue}/{secondValue}")
//    public void example(@PathVariable("firstValue") String firstValue,
//                        @PathVariable("secondValue") String secondValue) {
//        // ...
//    }

    @GetMapping("/doSomeParam/{firstValue}")
    public String doSomeParam(@PathVariable("firstValue") String firstValue){
        log.info("I am worked");
        return "Ok";
    }

    @GetMapping("/doSome")
    public String doSome(){
        log.info("I am worked");
        return "Ok";
    }

    @PostMapping("/doSome")
    public User doSomePost(@RequestBody User user){
        log.info("I am worked");
        return emailService.writeUserId(user);
    }

}
