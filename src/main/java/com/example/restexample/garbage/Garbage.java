package com.example.restexample.garbage;

import com.example.restexample.entity.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Garbage {
    public static void main(String[] args) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        User user = new User();
        user.setName("JuliaD");
        user.setEmail("juli@mail.ru");

        String s = mapper.writeValueAsString(user);
        log.info(s);
    }
}
