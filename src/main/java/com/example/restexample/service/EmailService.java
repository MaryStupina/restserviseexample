package com.example.restexample.service;

import com.example.restexample.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Random;

@Slf4j
@Component
public class EmailService {

    public User writeUserId(User user){
        int id = new Random().nextInt();
        user.setId(id);
        log.info("set id to user  {}", user);
        return user;
    }
}
